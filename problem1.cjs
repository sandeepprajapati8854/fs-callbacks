let fs = require("fs");

function createDirectoryTemp(callback) {
  fs.mkdir("./new-directory-name", (err) => {
    if (err) {
      callback(err);
    } else {
      console.log("directory created successfully.");
      callback(err);
    }
  });
}

function fileCreate(callback) {
  let checkError = 0;
  let checkFine = 0;
  for (let index = 1; index <= 5; index++) {
    fs.writeFile(
      `./new-directory-name/newFile${index}.json`,
      JSON.stringify({ name: "Sandeep", age: 25 }),
      (err) => {
        if (err) {
          checkError += 1;
        } else {
          checkFine += 1;
          console.log("File created");
        }
        if (checkError + checkFine === 5) {
          callback(err);
        }
      }
    );
  }
}

function deleteFile(callback) {
  let checkError = 0;
  let checkFine = 0;
  for (let index = 1; index <= 5; index++) {
    fs.unlink(`./new-directory-name/newFile${index}.json`, (err) => {
      if (err) {
        checkError += 1;
      } else {
        checkFine += 1;
        console.log("File deleted");
      }
      if (checkError + checkFine === 5) {
        callback(err);
      }
    });
  }
}

function calledFunction() {
  createDirectoryTemp((err) => {
    if (err) {
      console.log(err);
    } else {
      fileCreate((err) => {
        if (err) {
          console.log(err);
        } else {
          console.log("All file created successfully");

          deleteFile((err) => {
            if (err) {
              console.log(err);
            } else {
              console.log("All file deleted successfully");
            }
          });
        }
      });
    }
  });
}

module.exports = calledFunction;
