function parentFunction() {
  function readLipsum() {
    let fs = require("fs");

    fs.readFile("./lipsum.txt", (err, data) => {
      if (err) {
        console.log(err);
      } else {
        console.log(data); // print file.
        toUpperCase();
      }
    });
  }
  readLipsum(); // call the readLipsum

  function toUpperCase() {
    let content;
    let fs = require("fs");
    fs.readFile("./lipsum.txt", (err, data) => {
      if (err) {
        console.log(err);
      } else {
        content = data.toString().toUpperCase();
        let filename = "upperCaseFile.txt";
        fs.writeFile(`${filename}`, `${content}`, (err, data) => {
          if (err) {
            console.log(err);
          } else {
            // console.log("file converted to uppercase")
            fs.writeFile("filenames.txt", `${filename} `, (err, data) => {
              if (err) {
                console.log(err);
              } else {
                // console.log("FileName written to filenames.txt")
                lowerCase();
              }
            });
          }
        });
      }
    });
  }
  function lowerCase() {
    let lowerContent;
    let fs = require("fs");

    fs.readFile("./upperCaseFile.txt", (err, data) => {
      if (err) {
        console.log(err);
      } else {
        lowerContent = data.toString().toLowerCase();
        let filename1 = "lowerCaseFile.txt";
        fs.writeFile(`${filename1}`, `${lowerContent}`, (err, data) => {
          if (err) {
            console.log(err);
          } else {
            // console.log("file converted to lowercase")
            fs.readFile(`${filename1}`, (err, data) => {
              if (err) {
                console.log(err);
              } else {
                let arr = data.toString().split(". ");

                let sentenceFile = "sentenceFile.txt";
                fs.writeFile(
                  `${sentenceFile}`,
                  JSON.stringify(arr),
                  (err, data) => {
                    if (err) {
                      console.log(err);
                    } else {
                      // console.log('Converted the lowercase file to sentences and stored in sentences.txt')
                      fs.appendFile(
                        "filenames.txt",
                        `${sentenceFile} `,
                        (err, data) => {
                          if (err) {
                            console.log(err);
                          } else {
                            // console.log("filename written to filenames.txt")
                            fs.readFile("lowerCaseFile.txt", (err, data) => {
                              if (err) {
                                console.log(err);
                              } else {
                                let lowerArray = data
                                  .toString()
                                  .split(" ")
                                  .sort((first, second) => {
                                    return first.localeCompare(second);
                                  });
                                fs.writeFile(
                                  "sorted.txt",
                                  JSON.stringify(lowerArray),
                                  (err, data) => {
                                    if (err) {
                                      console.log(err);
                                    } else {
                                      fs.readFile(
                                        "upperCaseFile.txt",
                                        (err, data) => {
                                          if (err) {
                                            console.log(err);
                                          } else {
                                            let upperArray = data
                                              .toString()
                                              .split(" ")
                                              .sort((first, second) => {
                                                return first.localeCompare(
                                                  second
                                                );
                                              });
                                            fs.appendFile(
                                              "sorted.txt",
                                              JSON.stringify(upperArray),
                                              (err, data) => {
                                                if (err) {
                                                  console.log(err);
                                                } else {
                                                  // console.log("files sorted");
                                                  fs.appendFile(
                                                    "filenames.txt",
                                                    "sorted.txt",
                                                    (err, data) => {
                                                      if (err) {
                                                        console.log(err);
                                                      } else {
                                                        // console.log("filename added to filenames.txt");
                                                        fs.readFile(
                                                          "filenames.txt",
                                                          (err, data) => {
                                                            if (err) {
                                                              console.log(err);
                                                            } else {
                                                              let fileNameArray =
                                                                [];

                                                              fileNameArray =
                                                                data
                                                                  .toString()
                                                                  .split(" ");
                                                              for (
                                                                let index = 0;
                                                                index <
                                                                fileNameArray.length;
                                                                index++
                                                              ) {
                                                                fs.unlink(
                                                                  `${fileNameArray[index]}`,
                                                                  (
                                                                    err,
                                                                    data
                                                                  ) => {
                                                                    if (err) {
                                                                      console.log(
                                                                        err
                                                                      );
                                                                    } else {
                                                                      console.log(
                                                                        "deleted"
                                                                      );
                                                                    }
                                                                  }
                                                                );
                                                              }
                                                            }
                                                          }
                                                        );
                                                      }
                                                    }
                                                  );
                                                }
                                              }
                                            );
                                          }
                                        }
                                      );
                                    }
                                  }
                                );
                              }
                            });
                          }
                        }
                      );
                    }
                  }
                );
              }
            });
          }
        });
      }
    });
  }
}

module.exports = parentFunction;
